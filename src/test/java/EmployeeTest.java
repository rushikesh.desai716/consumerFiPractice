import main.java.Employee;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class EmployeeTest {

    private final Employee joey = new Employee("Joey", "Tribbiani", 30);
    private final Employee copyOfJoey = new Employee(joey);
    private final Employee ross = new Employee("Ross", "Geller", 32);

    @Test
    @DisplayName("CompareTo")
    void compareTo() {
        String msg = "Comparing whether two instances of employee are equal. Make use of Employee's copy constructor to achieve this.";
        assertAll(() -> assertEquals(0, joey.compareTo(copyOfJoey), msg),
                () -> assertNotEquals(0, joey.compareTo(ross)));
    }

    @Test
    @DisplayName("Employee equals()")
    void equals() {
        assertAll(() -> assertEquals(joey, copyOfJoey),
                () -> assertNotEquals(joey, ross));
    }

    @Test
    @DisplayName("Employee hashCode()")
    void hash_Code() {
        assertAll(() -> assertEquals(joey.hashCode(), copyOfJoey.hashCode()),
                () -> assertNotEquals(joey.hashCode(), ross.hashCode()));
    }

    @Test
    @DisplayName("Employee's getFirstName()")
    void getFirstName() {
        assertAll(() -> assertEquals("Joey", joey.getFirstName()),
                () -> assertNotEquals("Tribbiani", joey.getFirstName()),
                () -> assertEquals("Ross", ross.getFirstName()),
                () -> assertNotEquals("Geller", ross.getFirstName()));
    }

    @Test
    @DisplayName("Employee's getFirstName()")
    void getLastName() {
        assertAll(() -> assertNotEquals("Joey", joey.getLastName()),
                () -> assertEquals("Tribbiani", joey.getLastName()),
                () -> assertNotEquals("Ross", ross.getLastName()),
                () -> assertEquals("Geller", ross.getLastName()));
    }
}