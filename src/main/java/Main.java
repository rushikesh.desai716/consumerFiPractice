package main.java;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.DoubleConsumer;

public class Main {

    public static void main(String[] args) {

        PrintConsumer<String> stringPrintConsumer = Main::staticPrint;
        PrintConsumer<String> stringPrintConsumer1 = t -> System.out.print(t + " ");
        stringPrintConsumer.accept("Australia");
        stringPrintConsumer1.accept("Russia");

        PrintBiConsumer<String, Integer> printBiConsumer = Main::staticBiPrint;
        PrintBiConsumer<String, Double> printBiConsumer1 = (t, u) -> System.out.println(t + "\t" + u);
        printBiConsumer.accept("\nAdam", 30);
        printBiConsumer1.accept("\nAltroz", 12_00_000.00);

        PrintTriConsumer<String, String, Integer> printTriConsumer = Main::staticTriPrint;
        printTriConsumer.accept("\nAdam", "Gilchrist", 40);
        System.out.println();

        List<Employee> employees = new ArrayList<>(List.of(
                new Employee("Joey", "Tribbiani", 33),
                new Employee("Rachel", "Green", 30),
                new Employee("Chandler", "Bing", 25),
                new Employee("Monica", "Geller", 40),
                new Employee("Ross", "Geller", 23),
                new Employee("Phoebe", "Buffay", 33)
        ));
//        Collections.sort(employees);    //  main.java.Employee class must implement Comparable interface for Collections.sort() method to work.
        for (var employee : employees) {
            employee.accept(employee.getFirstName(), employee.getLastName(), employee.getAge());
        }
        System.out.println();
        employees.forEach(employee -> employee.accept(employee.getFirstName(), employee.getLastName(), employee.getAge()));
        System.out.println();
        employees.forEach(Main::staticTriPrint);

        //  Anonymous class instantiation on-the-fly.
        employees.sort(new Comparator<Employee>() {
            @Override
            public int compare(Employee e1, Employee e2) {
                return Integer.compare(e1.getId(), e2.getId());
            }
        });
        //  Anonymous class is replaced by a Lambda expression.
        employees.sort((e1, e2) -> Integer.compare(e1.getId(), e2.getId()));
        employees.sort(Comparator.comparingInt(Employee::getId));
        //  Lambda expression is replaced by a custom Method Reference.
        employees.sort(Employee::compareIds);
        employees.forEach(System.out::println);
        System.out.println();

        employees.sort((e1, e2) -> e1.getFirstName().compareTo(e2.getFirstName()));
        employees.sort(Comparator.comparing(Employee::getFirstName));
        employees.sort(Employee::compareFirstNames);
        employees.forEach(System.out::println);
        System.out.println();

        employees.sort(new Employee.EmployeeComparator("lastName"){});
        employees.sort(Employee::compareLastNames);
        employees.forEach(System.out::println);
        System.out.println();

        employees.sort(new Employee.EmployeeComparator("age"));
        employees.sort(Employee::compareAges);   //  Custom Method Reference
        employees.sort(Comparator.comparingInt(Employee::getAge));
        employees.forEach(System.out::println);
        System.out.println("-".repeat(50));

        Consumer<List<Integer>> modifyList = list -> list.replaceAll(element -> element << 2);//Multiplying each element in the list with 2 power 2, i.e. 4
        var myIntList = new ArrayList<>(List.of(2, 3, 4));
        modifyList.accept(myIntList);
        myIntList.forEach(System.out::println);
        Consumer<List<Integer>> intConsumer = Main::staticPrint;
        modifyList.andThen(intConsumer).accept(myIntList);      // <--- andThen() default method of Consumer interface.
        System.out.println("-".repeat(70));

        DoubleConsumer myIntConsumer = System.out::println;
        myIntConsumer.andThen(System.out::println).accept(10);

    }

    private static void staticTriPrint(Employee employee) {
        employee.accept(employee.getFirstName(), employee.getLastName(), employee.getAge());
    }

    private static <T> void staticPrint(T t) {
        System.out.print(t + " ");
    }

    private static <T, U> void staticBiPrint(T t, U u) {
        System.out.print(t + "\t" + u);
    }

    private static <T, U, V> void staticTriPrint(T t, U u, V v) {
        System.out.print(t + "\t" + u + "\t" + v);
    }
}

//  This is a Consumer. It accepts one parameter and returns nothing.
@FunctionalInterface
interface PrintConsumer<T> {
    void accept(T t);
}

//  This is a BiConsumer. It accepts two parameters and returns nothing.
@FunctionalInterface
interface PrintBiConsumer<T, U> {
    void accept(T t, U u);
}

//  This is a TriConsumer. It accepts three parameters and returns nothing.
@FunctionalInterface
interface PrintTriConsumer<T, U, V> {
    void accept(T t, U u, V v);
}